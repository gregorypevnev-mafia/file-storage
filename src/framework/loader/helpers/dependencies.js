const prepareUtils = (utils, dependencies) =>
  Object.keys(utils).reduce((fullUtils, utilName) => ({
    ...fullUtils,
    [utilName]: utils[utilName](dependencies),
  }), {});

module.exports = { prepareUtils };
