const emptyFunction = () => { };

const emptyObject = funcs => funcs.reduce((mapped, func) => ({
  ...mapped,
  [func]: emptyFunction,
}), {});

module.exports = { emptyFunction, emptyObject };