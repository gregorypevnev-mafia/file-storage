const extractTopics = events => Array.from(
  new Set(
    Object.keys(events).map(key => events[key].topic)
  )
);

const isEventValid = events => event => !!events[event];

const toTopic = events => event => events[event] ? events[event].topic : null;

module.exports = {
  extractTopics,
  isEventValid,
  toTopic,
};