const { EventEmitter } = require("events");
const debug = require("debug");

const eventLogger = debug("app").extend("system").extend("events");

const createDomainEvents = (mq, validator) => {
  const listeners = []; // Global listeners
  const emitter = new EventEmitter();

  mq.register((type, data) => {
    eventLogger("System-Event", type, data);

    if (validator(type)) emitter.emit(type, data);
  })

  return {
    // Listen to a specific event
    on(type, listener, errorListener = null) {
      emitter.on(type, async data => {
        try {
          eventLogger("Domain-Event", type, data);

          await listener(data);
        } catch (error) {
          // Note: Could additionally handle errors
          if (errorListener) errorListener(error);
        }
      });
    },
    // Listen to ALL Events
    register(listener) {
      listeners.push(listener);
    },
    async emit(type, payload) {
      if (!validator(type)) {
        eventLogger("Invalid Event", type);

        return;
      }

      eventLogger("Emitted", type, payload);

      // Notify Global Listeners
      listeners.forEach(listener => listener({ type, payload }));

      // IMPORTANT: Use MQ before emitting the event
      //  - Handler might publish an Event itself -> OUT-OF-ORDER
      await mq.send(type, payload);

      emitter.emit(type, payload);
    }
  }
}

module.exports = { createDomainEvents };
