const { extractToken } = require("./http");
const { encodeToken, decodeToken, invalidateToken } = require("./tokens");
const { defaultVerification } = require("./permissions");

const createAuthUtils = ({
  tokens,
  http
}) => ({
  extractToken: extractToken(http),
  encodeToken: encodeToken(tokens),
  decodeToken: decodeToken(tokens),
  invalidateToken: invalidateToken({}),
  defaultVerifyPermissions: defaultVerification,
});

module.exports = { createAuthUtils };