const createMeController = ({
  detailsForUser,
}) => {
  const me = ({ }) => async (req, res) => {
    const details = await Promise.resolve(detailsForUser(req.user));

    return res.status(200).json(details);
  };

  return {
    path: "/me",
    auth: true,
    private: true,
    use: {
      method: "GET",
      controller: me,
    }
  }
};

module.exports = { createMeController };
