const { createDefaultServer } = require("./server");
const { combineComponents } = require("./components");
const { createRoutes } = require("./routes");
const { errorHandler } = require("./errors");

const createApi = (components, dependencies, createServer, {
  apiPath,
  wsPath,
  handleErrors,
}) => {
  const app = createServer ? createServer() : createDefaultServer({ handleErrors });
  const { api, ws, middleware } = combineComponents(components, dependencies);

  app.use(apiPath, createRoutes(api, middleware, dependencies));
  app.use(wsPath, createRoutes(ws, middleware, dependencies));

  if (handleErrors) {
    app.use(errorHandler); // Default Error-Handler (When error is passed to "next")
    // IMPORTANT: Does NOT handle throws -> Only works with unhandled "next" errors => Serves as a fallback
  }

  return app;
};

module.exports = { createApi };