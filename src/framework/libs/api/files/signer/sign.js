const { hashFile } = require("./hasher");

const createSigner = ({ secret, ttl, allowedExtensions }) =>
  (file, user) => {
    const [filename, extension] = file.split(".");
    const expires = Date.now() + ttl;

    if (allowedExtensions.indexOf(extension) === -1) throw { message: "Invalid type of a file" };

    const signature = hashFile({ filename, extension, user, secret, expires });

    return { signature, expires };
  };

module.exports = {
  createSigner,
};
