const socketUserKey = socketId => `SOCKET_${socketId}_USER`;
const socketRoomKey = socketId => `SOCKET_${socketId}_ROOM`;

const userKey = userId => `USER_${userId}`;

const roomKey = roomId => `ROOM_${roomId}`;

const createDatabase = (client) => {
  const save = async ({ socketId, userId, roomId }) => {
    await Promise.all([
      client.set(userKey(userId), socketId),
      client.set(socketUserKey(socketId), userId),
      client.set(socketRoomKey(socketId), roomId),
      client.lpush(roomKey(roomId), socketId),
    ]);
  };

  const remove = async socketId => {
    const [userId, roomId] = await Promise.all([
      client.get(socketUserKey(socketId)),
      client.get(socketRoomKey(socketId)),
    ]);

    await Promise.all([
      client.del(userKey(userId)),
      client.del(socketUserKey(socketId)),
      client.del(socketRoomKey(socketId)),
      client.lrem(roomKey(roomId), 0, socketId),
    ]);
  };

  const findForUser = async userId => {
    const socket = await client.get(userKey(userId));

    return socket || null;
  };

  const findForRoom = async roomId => {
    const sockets = await client.lrange(roomKey(roomId), 0, -1);

    return sockets || null;
  };

  return {
    save,
    remove,
    findForUser,
    findForRoom,
  }
}

module.exports = { createDatabase };
