const { v1: uuid } = require("uuid");
const debug = require("debug");
const { createStore } = require("./store");
const { createSockets } = require("./sockets");

const messengerLogger = debug("app").extend("system").extend("messenger");

const generateId = () => String(uuid());

const createMessenger = ({
  readClient,
  writeClient
}, {
  pipes: {
    input, // Input-Messages: Mapping Messages to Events
    output, // Output-Events: Mapping Events to Messages
  },
  events: {
    // Pre-Defined Events
    connectedEvent,
    disconnectedEvent,
  },
}, messengerConfig, onEvent) => {
  let sockets; // IMPORTANT: Fixing ordering problems

  const store = createStore({
    readClient,
    writeClient,
  }, {
    async onMessage(message) {
      const { to, ...data } = message;

      if (to === null) sockets.broadcast(data);
      else await sockets.send(to, data);

      messengerLogger("Message Sent:", message);
    },
    onClose(socket) {
      sockets.remove(socket);

      messengerLogger(`Web-Socket "${socket}" Closed`);
    }
  }, messengerConfig);

  const eventToMessage = async (type, payload) => {
    messengerLogger(`Event to Message`, type, payload);

    const messageMapper = output[type];
    if (!messageMapper) {
      messengerLogger(`NOT an Output-Event`, type);

      return; // NOT AN OUTPUT-EVENT
    }

    const message = await Promise.resolve(messageMapper(payload));

    if (message) store.send(message); // { type, payload, [user / role / room] }
  };

  const messageToEvent = async (type, {
    payload,
    room,
    user
  }) => {
    messengerLogger(`Message to Event`, type, payload, room, user);

    const eventMapper = input[type];

    if (!eventMapper) {
      messengerLogger(`NOT an Input-Message`, type);

      return; // NOT an Input-Message
    }

    const event = await Promise.resolve(eventMapper({ payload, user, room }));

    if (event !== null) onEvent(event); // Outputting (Sending to Domain-Events)
    else messengerLogger(`Invalid message`, type, payload, user, room);
  };

  sockets = createSockets({
    async onMessage(message, socketId, { user, room }) {
      messengerLogger(`Incoming message from Socket "${socketId}" (User: ${user}, Room: ${room})`, message);

      const { type, payload } = message;

      await messageToEvent(type, {
        payload,
        user,
        room,
      });
    },
    async onClose(socketId, { user, room }) {
      await store.disconnectSocket(socketId);

      await messageToEvent(disconnectedEvent, {
        payload: { at: Date.now() },
        user,
        room,
      });

      messengerLogger(`Disconnected: Socket(${socketId}), User(${user}), Room(${room})`);
    }
  });

  const connect = async (socket, userId, roomId) => {
    const socketId = generateId();

    // Remove previous connection
    await store.disconnectUser(userId);
    // Not necessary
    // if (disconnectedId) sockets.remove(disconnectedId); // Do immediately

    await store.connect(socketId, userId, roomId);

    // Attaching details for easier access
    sockets.add(socket, socketId, {
      user: userId,
      room: roomId,
    });

    await messageToEvent(connectedEvent, {
      payload: { at: Date.now() },
      user: userId,
      room: roomId,
    });

    messengerLogger(`Connected: Socket(${socketId}), User(${userId}), Room(${roomId})`);

    return socketId;  // May not be necessary
  };

  const message = ({ type, payload }) => {
    eventToMessage(type, payload)
  };

  const initialize = async () => {
    await store.initialize();
  };

  return {
    connect,
    message,
    initialize,
  };
}

module.exports = { createMessenger };
