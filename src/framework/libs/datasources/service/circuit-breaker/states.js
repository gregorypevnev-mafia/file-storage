exports.OPEN = "OPEN"; // Breaker is opened -> NOT making any requests

exports.CLOSED = "CLOSED"; // Breaker is closed -> Making real requests

exports.PARTIAL = "PARTIAL"; // Open just for a single request (To test if everything is OK)
// Note: Does NOT affect logic - Simply usedd for semantics
//  -> COULD affect logic if Partially-Open allowed a number of retries (NOT just a single Failure-Threshold)