const { microserviceBuilder } = require("./framework");

(async function () {
  const microservice = await microserviceBuilder()
    .withEvents(require("./app/events.json"))
    .withSchemas()
    .useFiles()
    .useInfo()
    .build();

  microservice.start();
}());
