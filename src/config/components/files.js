const DEFAULT_SIGNING_SECRET = "SECRET";
const SIGNING_SECRET = String(process.env.SIGNING_SECRET || DEFAULT_SIGNING_SECRET);

const DEFAULT_SIGNATURE_TTL = 60 * 1000;
const SIGNATURE_TTL = Number(process.env.SIGNATURE_TTL || DEFAULT_SIGNATURE_TTL);

const DEFAULT_STATIC_FILES = "/Users/mac/Desktop/Mafia/system/files/static";
const STATIC_FILES = String(process.env.STATIC_FILES || DEFAULT_STATIC_FILES);

const DEFAULT_DYNAMIC_FILES = "/Users/mac/Desktop/Mafia/system/files/dynamic";
const DYNAMIC_FILES = String(process.env.DYNAMIC_FILES || DEFAULT_DYNAMIC_FILES);

module.exports = {
  secret: SIGNING_SECRET,
  paths: {
    static: STATIC_FILES,
    dynamic: DYNAMIC_FILES
  },
  ttl: SIGNATURE_TTL,
  allowedExtensions: ["jpg", "png"],
  fileField: "file",
}