module.exports = {
  basePath: "/api",
  handleErrors: true,

  auth: require("./auth"),

  ws: require("../common/ws"),
};