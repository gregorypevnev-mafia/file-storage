module.exports = {
  // Custom
  app: require("./components/app"),

  info: require("./components/info"),

  files: require("./components/files"),

  // Microservice-Specific
  api: require("./components/api"),

  core: require("./components/core"),

  datasources: require("./components/datasources"),

  messaging: require("./components/messaging"),

  server: require("./components/server"),
};