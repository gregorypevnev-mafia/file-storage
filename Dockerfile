FROM node:alpine

# No Building - Allows debugging at real-time

WORKDIR /app

# Build utilities (Bcrypt / Hashing)
RUN apk add g++ make python

RUN apk add curl

RUN npm install -g pm2

COPY package.json /app/package.json

RUN npm install --only=production

# Copy to root-path (For Configuration)
COPY ./scripts/running /app/
COPY ./src/ /app/

# Note: Using Volume for Dynamic-Files
# Additional: Can use Read-Only Volume for Static-Files
ENV STATIC_FILES=/data/files/static
ENV DYNAMIC_FILES=/data/files/dynamic

VOLUME /data/files/static
VOLUME /data/files/dynamic

ENV DEBUG=app:*
ENV NODE_ENV=production

HEALTHCHECK --interval=60s --timeout=10s --start-period=5s --retries=3 CMD curl --fail "http://localhost:$PORT/api/info/health" || exit 1

CMD ["sh", "run.sh"]