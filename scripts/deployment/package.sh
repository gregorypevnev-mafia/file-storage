#!/bin/bash

if [ -z "$NAME" ] || [ -z "$PROJECT" ] || [ -z "$REGISTRY" ]
then
  echo "Docker information not provided"
  exit 1
fi

IMAGE="$REGISTRY/$PROJECT/$NAME"

echo "Image: $IMAGE"

docker image build -t $IMAGE .

docker image push $IMAGE